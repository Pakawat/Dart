class Person {
  String name;
  int age;
  double height;

  Person(this.name, this.age, this.height);

  void printDescription() {
    print("My name is $name. I'm $age years old, and I'm $height meters tall.");
  }
}

void main() {
  final person = Person('Andrea', 36, 1.84);
  person.printDescription();
}
