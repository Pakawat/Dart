class Student {
  late String _name;
  late String _surname;
  late double _gpa;

  // Getter for name
  String get studentName {
    return _name;
  }

  // Setter for name
  set studentName(String newName) {
    _name = newName;
  }

  // Getter for surname
  String get studentSurname {
    return _surname;
  }

  // Setter for surname
  set studentSurname(String newSurname) {
    _surname = newSurname;
  }

  // Getter for GPA
  double get studentGPA {
    return _gpa;
  }

  // Setter for GPA
  set studentGPA(double newGPA) {
    _gpa = newGPA;
  }
}

void main() {
  // Creating a new student object
  var student = Student();

  // Setting values using setters
  student.studentName = "Pakawat";
  student.studentSurname = "Promeam";
  student.studentGPA = 3.8;

  // Getting values using getters
  print("Name: ${student.studentName}");
  print("Surname: ${student.studentSurname}");
  print("GPA: ${student.studentGPA}");
}
