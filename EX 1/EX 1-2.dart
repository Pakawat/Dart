import 'dart:io';

void main() {
  double tempFarenheit;

  print("Enter your temperature farenheit... ");
  tempFarenheit = double.parse(stdin.readLineSync()!);
  // Fahrenheit to Celsius:   (°F − 32) / 1.8 = °C
  print("${tempFarenheit.toInt()}F = ${(tempFarenheit - 32) / 1.8}C");
}