import 'dart:io';

main() {
  print("Input your character...");
  String? character = stdin.readLineSync();
  String char =character!.toLowerCase();

  bool charIsAlphabetic = RegExp(r'^[a-zA-Z]$').hasMatch(char);

  if (character.length == 1 && charIsAlphabetic == true) {
    if (char == 'a' || char == 'e' || char == 'i' || char == 'o' || char == 'u') {
      print('${character[0]} is vowel.');
    } else {
      print('${character[0]} is consonant.');
    }
  } else if (character.length > 1) {
    print("Please enter 1 character.");
  } else if (charIsAlphabetic == false) {
    print("Please enter English character.");
  } else if (character.length == 0) {
    print("Please enter a valid character.");
  }
}

