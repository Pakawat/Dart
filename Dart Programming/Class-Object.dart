class Laptop {
  void show() {
    print("Laptop display.");
  }
}

class MacBook extends Laptop {
  @override
  void show() {
    print("MacBook display.");
    super.show();
  }
}

class MacBookPro extends MacBook {
  @override
  void show() {
    print("MacBookPro display.");
    super.show();
  }
}

void main() {
  MacBookPro macbookpro = MacBookPro();
  macbookpro.show();
}
