void main() {
  printInfo(name: "John", gender: "Male");
  printInfo(name: "Sita", gender: "Female");
  printInfo(name: "Preeda", gender: "Female");
  printInfo(name: "Harry", gender: "Male");
  printInfo(name: "Santa", gender: "Male");
}

void printInfo({String? name, String? gender}) {
  print("Hello $name your gender is $gender.");
}
